--CLIENTS TABLE

drop table clients

create type id_type as enum ('INE', 'PASAPORTE')

create table clients(
	client_id int generated always as identity,
	name varchar(20) not null,
	last_name varchar(20) not null,
	id varchar(20) not null,
	id_type id_type,
	address varchar(255) not null,
	email varchar(60) not null,
	phone varchar(10) not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(client_id)
)

insert into clients(name, last_name, id, id_type, address, email, phone) values('Jesus', 'Cadena', '123456MEX', 'INE', 'my address', 'jesus@gmail.com', '5566778899')

insert into clients(name, last_name, id, id_type, address, email, phone) values('Jose', 'Chavez', '123457MEX', 'INE', 'my address', 'jose@gmail.com', '5566778890')

select * from clients 

-- BRANDS TABLE

drop table brands

create table brands(
	brand_id int generated always as identity,
	name varchar(30) not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(brand_id)
)

insert into brands(name) values('Mazda')

update brands set name = 'MAZDA', updated_at = current_timestamp where brand_id = 1

select * from brands

-- MODELS TABLE

drop table models

create table models(
	model_id int generated always as identity,
	name varchar(30) not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(model_id)
)

insert into models(name) values('CX-30')

delete from models where model_id = 1

select * from models

-- CARS TABLE

drop table cars

create table cars(
	car_id int generated always as identity,
	year varchar(4) not null,
	plate varchar(10) not null,
	rent_pricing float not null,
	no_serie varchar(30) not null,
	is_rented boolean default false,
	brand_id int not null,
	model_id int not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(car_id),
	constraint fk_model
	foreign key(model_id)
	references models(model_id)
	on delete cascade
	on update cascade,
	constraint fk_brand
	foreign key(brand_id)
	references brands(brand_id)
	on delete cascade
	on update cascade
)

insert into cars(year, plate, rent_pricing, no_serie, brand_id, model_id) values('2021', 'DATA-123', 100, '123456', 1, 2)

insert into cars(year, plate, rent_pricing, no_serie, brand_id, model_id) values('2021', 'DATA-124', 100, '123457', 1, 2)

delete from cars where car_id = 1

select * from cars

--BRANCH OFFICES TABLE

drop table branch_offices

create table branch_offices(
	branch_office_id int generated always as identity,
	name varchar(40) not null,
	address varchar(255) not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(branch_office_id)
)

insert into branch_offices(name, address) values('Branch 1', 'CDMX'), ('Branch 2', 'Puebla')

select * from branch_offices 

-- RENTS TABLE

drop type rent_type

create type trip_type as enum ('local', 'foreign')

create type status_type as enum ('opened', 'closed')

drop table rents

create table rents(
	rent_id int generated always as identity,
	start_date timestamp not null default current_timestamp,
	end_date timestamp not null,
	trip_type trip_type,
	status status_type default 'opened',
	client_id int not null,
	car_id int not null,
	departure int not null,
	arrival int default null,
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	is_deleted boolean default false,
	primary key(rent_id),
	constraint fk_departure
	foreign key(departure)
	references branch_offices(branch_office_id) 
	on delete cascade 
	on update cascade,
	constraint fk_arrival
	foreign key(arrival)
	references branch_offices(branch_office_id) 
	on delete cascade 
	on update cascade
)

truncate table rents

insert into rents(end_date, trip_type, client_id, car_id, departure) values('2021/03/15 12:00:00', 'local', 1, 5, 1)

insert into rents(end_date, trip_type, client_id, car_id, departure) values('2021/03/16 12:00:00', 'foreign', 2, 6, 2)

insert into rents(end_date, trip_type, client_id, car_id, departure) values('2021/03/25 12:00:00', 'foreign', 2, 6, 2)

delete from rents where rent_id = 2

select * from rents

--JOINS

select r.rent_id, c.name, c.last_name, m.name, b.name, r.status, r.trip_type
from rents r
left join clients c
on r.client_id = c.client_id
left join cars ca
on r.car_id = ca.car_id
left join models m
on ca.model_id = m.model_id
left join brands b
on ca.brand_id = b.brand_id
