CREATE OR REPLACE PROCEDURE public.sp_merge_cars_dimension()
LANGUAGE PLPGSQL
AS $$
	BEGIN

	--CARS DIMENSION
	CREATE TABLE IF NOT EXISTS dwh.cars (
	    car_id INTEGER,
		year VARCHAR(4),
		plate VARCHAR(10),
		no_serie VARCHAR(30),
		brand_id INTEGER,
		model_id INTEGER
	);

	CREATE TABLE IF NOT EXISTS dwh.stg_cars (
        car_id INTEGER,
		year VARCHAR(4),
		plate VARCHAR(10),
		rent_pricing FLOAT,
		no_serie VARCHAR(30),
		is_rented BOOLEAN,
		brand_id INTEGER,
		model_id INTEGER,
		created_at VARCHAR(30),
		updated_at VARCHAR(30),
		is_deleted BOOLEAN
	);

	TRUNCATE TABLE dwh.stg_cars;
    
    COPY dwh.stg_cars
	FROM 's3://jm-fondeadora/cars/'
	FORMAT AS PARQUET
	CREDENTIALS 'aws_iam_role=arn:aws:iam::479967276964:role/RootRedshiftRole';

	CREATE TEMP TABLE cars_insert AS(
		SELECT car_id, year, plate, no_serie, brand_id, model_id
		FROM(
			SELECT
			stg.car_id,
			stg.year,
			stg.plate,
			stg.no_serie,
			stg.brand_id,
			stg.model_id,
			ROW_NUMBER() OVER (PARTITION BY stg.car_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_cars stg
			LEFT JOIN dwh.cars final
			ON stg.car_id = final.car_id
			WHERE final.car_id IS NULL
		)
		WHERE row = 1
	);

	CREATE TEMP TABLE cars_update AS(
		SELECT car_id, year, plate, no_serie, brand_id, model_id
		FROM(
			SELECT
			stg.car_id,
			stg.year,
			stg.plate,
			stg.no_serie,
			stg.brand_id,
			stg.model_id,
			ROW_NUMBER() OVER (PARTITION BY stg.car_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_cars stg
			LEFT JOIN dwh.cars final
			ON stg.car_id = final.car_id
			WHERE final.car_id IS NOT NULL
		)
		WHERE row = 1
	);

	INSERT INTO dwh.cars(
		SELECT
		car_id,
		year,
		plate,
		no_serie,
		brand_id,
		model_id
		FROM cars_insert
	);

	UPDATE dwh.cars final
	SET
	year = src.year,
	plate = src.plate,
	no_serie = src.no_serie,
	brand_id = src.brand_id,
	model_id = src.model_id
	FROM cars_update src
	WHERE src.car_id = final.car_id;
    
    END;
$$

CALL public.sp_merge_cars_dimension()