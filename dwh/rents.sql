CREATE OR REPLACE PROCEDURE public.sp_merge_rents_fact()
LANGUAGE PLPGSQL
AS $$
	BEGIN

	--RENTS FACT
	CREATE TABLE IF NOT EXISTS dwh.rents (
	    rent_id INTEGER,
		car_id INTEGER,
		model VARCHAR(30),
		brand VARCHAR(30),
		num_days INTEGER
	);

	CREATE TABLE IF NOT EXISTS dwh.stg_rents (
        rent_id INTEGER,
		start_date VARCHAR(30),
		end_date VARCHAR(30),
		trip_type VARCHAR(10),
		status VARCHAR(10),
		client_id INTEGER,
		car_id INTEGER,
		departure INTEGER,
		arrival INTEGER,
		created_at VARCHAR(30),
		updated_at VARCHAR(30),
		is_deleted BOOLEAN
	);

	TRUNCATE TABLE dwh.stg_rents;
    
    COPY dwh.stg_rents
	FROM 's3://jm-fondeadora/rents/'
	FORMAT AS PARQUET
	CREDENTIALS 'aws_iam_role=arn:aws:iam::479967276964:role/RootRedshiftRole';

	CREATE TEMP TABLE rents_insert AS(
		SELECT
		ri.rent_id,
		ri.car_id,
		m.name AS model,
		b.name AS brand,
		DATEDIFF(days, CAST(ri.start_date AS TIMESTAMP), CAST(ri.end_date AS TIMESTAMP)) AS num_days
		FROM(
			SELECT
			stg.rent_id,
			stg.car_id,
			stg.start_date,
			stg.end_date,
			ROW_NUMBER() OVER (PARTITION BY stg.rent_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_rents stg
			LEFT JOIN dwh.rents final
			ON stg.rent_id = final.rent_id
			WHERE final.rent_id IS NULL
		) ri
		LEFT JOIN dwh.cars c --ENRICHMENT
		ON ri.car_id = c.car_id
		LEFT JOIN dwh.brands b --ENRICHMENT
		ON c.brand_id = b.brand_id
		LEFT JOIN dwh.models m --ENRICHMENT
		ON c.model_id = m.model_id
		WHERE row = 1
	);

	CREATE TEMP TABLE rents_update AS(
		SELECT
		ri.rent_id,
		ri.car_id,
		m.name AS model,
		b.name AS brand,
		DATEDIFF(days, CAST(ri.start_date AS TIMESTAMP), CAST(ri.end_date AS TIMESTAMP)) AS num_days
		FROM(
			SELECT
			stg.rent_id,
			stg.car_id,
			stg.start_date,
			stg.end_date,
			ROW_NUMBER() OVER (PARTITION BY stg.rent_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_rents stg
			LEFT JOIN dwh.rents final
			ON stg.rent_id = final.rent_id
			WHERE final.rent_id IS NOT NULL
		) ri
		LEFT JOIN dwh.cars c --ENRICHMENT
		ON ri.car_id = c.car_id
		LEFT JOIN dwh.brands b --ENRICHMENT
		ON c.brand_id = b.brand_id
		LEFT JOIN dwh.models m --ENRICHMENT
		ON c.model_id = m.model_id
		WHERE row = 1
	);

	INSERT INTO dwh.rents(
		SELECT
		rent_id,
		car_id,
		model,
		brand,
		num_days
		FROM rents_insert
	);

	UPDATE dwh.rents final
	SET
	car_id = src.car_id,
	model = src.model,
	brand = src.brand,
	num_days = src.num_days
	FROM rents_update src
	WHERE src.rent_id = final.rent_id;
    
    END;
$$

CALL public.sp_merge_rents_fact()