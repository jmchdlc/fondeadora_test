CREATE MATERIALIZED VIEW mv_avg_rented_day
AUTO REFRESH YES
AS
SELECT
brand,
model,
AVG(num_days) AS avg_rented_days
FROM dwh.rents
GROUP BY brand, model


SELECT * 
FROM mv_avg_rented_day
ORDER BY avg_rented_days DESC
LIMIT 10