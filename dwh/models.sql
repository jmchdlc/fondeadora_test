CREATE OR REPLACE PROCEDURE public.sp_merge_models_dimension()
LANGUAGE PLPGSQL
AS $$
	BEGIN

	--MODELS DIMENSION
	CREATE TABLE IF NOT EXISTS dwh.models (
	    model_id INTEGER,
		name VARCHAR(30)
	);

	CREATE TABLE IF NOT EXISTS dwh.stg_models (
        model_id INTEGER,
		name VARCHAR(30),
		created_at VARCHAR(30),
		updated_at VARCHAR(30),
		is_deleted BOOLEAN
	);

	TRUNCATE TABLE dwh.stg_models;
    
    COPY dwh.stg_models
	FROM 's3://jm-fondeadora/models/'
	FORMAT AS PARQUET
	CREDENTIALS 'aws_iam_role=arn:aws:iam::479967276964:role/RootRedshiftRole';

	CREATE TEMP TABLE models_insert AS(
		SELECT model_id, name
		FROM(
			SELECT
			stg.model_id,
			stg.name,
			ROW_NUMBER() OVER (PARTITION BY stg.model_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_models stg
			LEFT JOIN dwh.models final
			ON stg.model_id = final.model_id
			WHERE final.model_id IS NULL
		)
		WHERE row = 1
	);

	CREATE TEMP TABLE models_update AS(
		SELECT model_id, name
		FROM(
			SELECT
			stg.model_id,
			stg.name,
			ROW_NUMBER() OVER (PARTITION BY stg.model_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_models stg
			LEFT JOIN dwh.models final
			ON stg.model_id = final.model_id
			WHERE final.model_id IS NOT NULL
		)
		WHERE row = 1
	);

	INSERT INTO dwh.models(
		SELECT
		model_id,
		name
		FROM models_insert
	);

	UPDATE dwh.models final
	SET
	name = src.name
	FROM models_update src
	WHERE src.model_id = final.model_id;
    
    END
$$

CALL public.sp_merge_models_dimension()