CREATE OR REPLACE PROCEDURE public.sp_merge_brands_dimension()
LANGUAGE PLPGSQL
AS $$
	BEGIN

	--BRANDS DIMENSION
	CREATE TABLE IF NOT EXISTS dwh.brands (
	    brand_id INTEGER,
		name VARCHAR(30)
	);

	CREATE TABLE IF NOT EXISTS dwh.stg_brands (
        brand_id INTEGER,
		name VARCHAR(30),
		created_at VARCHAR(30),
		updated_at VARCHAR(30),
		is_deleted BOOLEAN
	);

	TRUNCATE TABLE dwh.stg_brands;
    
    COPY dwh.stg_brands
	FROM 's3://jm-fondeadora/brands/'
	FORMAT AS PARQUET
	CREDENTIALS 'aws_iam_role=arn:aws:iam::479967276964:role/RootRedshiftRole';

	CREATE TEMP TABLE brands_insert AS(
		SELECT brand_id, name
		FROM(
			SELECT
			stg.brand_id,
			stg.name,
			ROW_NUMBER() OVER (PARTITION BY stg.brand_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_brands stg
			LEFT JOIN dwh.brands final
			ON stg.brand_id = final.brand_id
			WHERE final.brand_id IS NULL
		)
		WHERE row = 1
	);

	CREATE TEMP TABLE brands_update AS(
		SELECT brand_id, name
		FROM(
			SELECT
			stg.brand_id,
			stg.name,
			ROW_NUMBER() OVER (PARTITION BY stg.brand_id ORDER BY CAST(stg.updated_at AS timestamp) DESC) AS row
			FROM dwh.stg_brands stg
			LEFT JOIN dwh.brands final
			ON stg.brand_id = final.brand_id
			WHERE final.brand_id IS NOT NULL
		)
		WHERE row = 1
	);

	INSERT INTO dwh.brands(
		SELECT
		brand_id,
		name
		FROM brands_insert
	);

	UPDATE dwh.brands final
	SET
	name = src.name
	FROM brands_update src
	WHERE src.brand_id = final.brand_id;
    
    END
$$

CALL public.sp_merge_brands_dimension()